﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelDisplay : MonoBehaviour
{

    protected float Timer;

    // Use this for initialization
    void Start()
    {
        int currentLevel = PlayerPrefs.GetInt("Current Level", 0) + 1;
        GetComponent<Text>().text = "Level " + currentLevel;
        Timer = 3.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Timer -= Time.fixedDeltaTime;

        if (Timer < 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
