﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {

    public float MinDamage = 20.0f;
    public float MaxDamage = 30.0f;
    public float LifeTime = -1;
    public bool IsTrap = true;

    protected Player Player;
    protected float LifeTimer;

	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        LifeTimer = LifeTime;
        
        Player.PlaceTrap();
	}
	
	void FixedUpdate () {
	    if (LifeTimer > 0)
        {
            LifeTimer -= Time.fixedDeltaTime;
            if (LifeTimer <= 0.0f)
            {
                Player.ReleaseTrap();
                Destroy(gameObject);
            }
        }
	}

    void OnTriggerEnter(Collider col)
    {
        if (Player == null)
        {
            return; // Not yet initialized
        }
        Opponent opponent = col.gameObject.GetComponent<Opponent>();
        if (col.gameObject.CompareTag("Enemy") && opponent != null)
        {
            if (!opponent.DisablesTrap)
            {
                float damage = Random.Range(MinDamage, MaxDamage);
                opponent.Damage(damage);
            }
            Player.ReleaseTrap();
            Destroy(gameObject);
        }
    }
}
