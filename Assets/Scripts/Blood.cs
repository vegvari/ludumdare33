﻿using UnityEngine;
using System.Collections;

public class Blood : MonoBehaviour
{

    protected float LifeTimer;

    // Use this for initialization
    void Start()
    {
        LifeTimer = 10.0f;
    }

    void FixedUpdate()
    {
        LifeTimer -= Time.fixedDeltaTime;
        if (LifeTimer <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
