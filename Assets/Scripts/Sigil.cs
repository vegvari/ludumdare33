﻿using UnityEngine;
using System.Collections;

public class Sigil : MonoBehaviour
{
    public float AttackIntervals = 0.5f;
    public float Radius = 20.0f;
    public float MinDamage = 20.0f;
    public float MaxDamage = 30.0f;
    public float LifeTime = -1;
    public float FadeInTime = 2.0f;
    public float HealingFactor = 0.25f;

    protected float FadeTime;
    protected float LifeTimer;
    protected float AttackTimer;
    protected GameObject Player;
    protected GameObject Map;

    // Use this for initialization
    void Start()
    {
        Map = GameObject.FindGameObjectWithTag("Map");
        Player = GameObject.FindGameObjectWithTag("Player");
        LifeTimer = LifeTime;
        FadeTime = FadeInTime;
        //transform.localScale = new Vector3(Radius * 0.01f, 1.0f, Radius * 0.01f);

        Renderer renderer = GetComponentInChildren<Renderer>();
        renderer.material.color = new Color(0,0,0,0);
    }

    void FixedUpdate()
    {
        if (Player == null)
        {
            Start();
        }

        if (FadeTime > 0.0f)
        {
            FadeTime -= Time.fixedDeltaTime;
            float fadeFactor = 1.0f - (FadeTime / FadeInTime);
            //transform.localScale = new Vector3(Radius * fadeFactor, 1.0f, Radius * fadeFactor);
            Renderer renderer = GetComponentInChildren<Renderer>();
            renderer.material.color = new Color(fadeFactor, fadeFactor, fadeFactor, fadeFactor);

            return;
        }

        if (LifeTimer > 0)
        {
            LifeTimer -= Time.fixedDeltaTime;
            if (LifeTimer <= 0.0f)
            {
                Destroy(gameObject);
            }
        }

        AttackTimer -= Time.fixedDeltaTime;

        if (AttackTimer < 0.0f)
        {
            AttackTimer = AttackIntervals;

            DoAttack();
        }
        if (Player != null && (Player.transform.position - transform.position).sqrMagnitude < Radius * Radius)
        {
            Player.GetComponent<Player>().Slow();
        }
    }

    void DoAttack()
    {
        float damage = Random.Range(MinDamage, MaxDamage);

        if (Player != null && (Player.transform.position - transform.position).sqrMagnitude < Radius * Radius)
        {
            Player.GetComponent<Creature>().Damage(damage);
        }

        foreach (GameObject opponent in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if ((opponent.transform.position - transform.position).sqrMagnitude < Radius * Radius)
            {
                Creature other = opponent.GetComponent<Creature>();

                if (other != null)
                {
                    other.Heal(damage * HealingFactor);
                }
            }
        }
    }
}
