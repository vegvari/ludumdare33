﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Guide : MonoBehaviour
{
    public float Speed = 1.0f;

    protected float ExplorationTimer = 0.0f;
    protected GameObject Map;
    protected Map MapScript;
    protected List<Creature> Members = new List<Creature>();
    protected List<Vector3> Path = new List<Vector3>();
    protected bool Valid;
    protected Vector3 TargetPosition;
    internal int Dead;

    public bool IsValid()
    {
        return Valid;
    }

    // Use this for initialization
    void Start()
    {
        Dead = 0;
        Map = GameObject.FindGameObjectWithTag("Map");
        MapScript = Map.GetComponent<Map>();

        TargetPosition = transform.position;

        Valid = true;
    }

    void FixedUpdate()
    {
        if (Dead >= Members.Count && Members.Count > 0)
        {
            GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().Finish(true);
            Dead = -9999;
        }

        ExplorationTimer -= Time.fixedDeltaTime;

        if (ExplorationTimer < 0.0f)
        {
            ExplorationTimer = 0.5f;

            MapScript.Explore(Members);
        }

        Vector3 targetDirection = (TargetPosition - transform.position);
        targetDirection.y = 0;

        if (targetDirection.sqrMagnitude > 0.1f)
        {
            float moveSpeed = Speed * Time.fixedDeltaTime;

            if (targetDirection.sqrMagnitude > moveSpeed * moveSpeed)
            {
                targetDirection.Normalize();
                GetComponent<CharacterController>().Move(targetDirection * moveSpeed);
            }
            else
            {
                GetComponent<CharacterController>().Move(targetDirection);
            }
        }
        else
        {
            if (Path.Count > 0)
            {
                for (int l = 0; l < Path.Count - 1; ++l)
                {
                    Vector3 dir = Path[l] - transform.position;
                    dir.y = 0.0f;
                    float distance = dir.magnitude;
                    dir.Normalize();
                    Ray ray = new Ray(transform.position + 2 * Vector3.up, dir);
                    RaycastHit hitInfo;

                    if (!Physics.SphereCast(ray, 1.5f, out hitInfo, distance, 1))
                    {
                        Path.RemoveRange(l + 1, Path.Count - l - 1);
                        break;
                    }
                }

                TargetPosition = Path[Path.Count - 1];
                Path.RemoveAt(Path.Count - 1);
            }
        }
    }

    public void AddMember(GameObject gameObject)
    {
        Opponent opponent = gameObject.GetComponent<Opponent>();
        Members.Add(opponent);
        opponent.SetGuid(this);
    }

    internal void SetPath(List<Vector3> path)
    {
        Path = path;
    }
}
