﻿using UnityEngine;
using System.Collections;

public class Crypt : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        Opponent opponent = col.gameObject.GetComponent<Opponent>();
        if (col.gameObject.CompareTag("Enemy") && opponent != null)
        {
            Destroy(gameObject);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().Damage(1000000);
            GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().Finish(false);
        }
    }
}
