﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Creature : MonoBehaviour
{
    public float Speed = 0.3f;
    public float MaxHealth = 100.0f;
    public float DamageMin = 20.0f;
    public float DamageMax = 30.0f;
    public float AttackTime = 01.25f;
    public float AttackDistance = 10.0f;
    public float AttackTimeout = 2.0f;
    public GameObject Projectile = null;
    public float ProjectileSpeed = 15.0f;
    public float InvisibilityTime = 5.0f;
    public float InvisibilityTimeout = 30.0f;
    public float SightDistance = 60.0f;
    public bool CanSeeInvisible = false;
    public GameObject SigilRune;
    public float SigilTimeout = 30.0f;
    public float VampireFactor = 0.0f;
    public AudioClip[] AttackSounds;
    public AudioClip[] HurtSounds;
    public AudioClip DeathSound;
    public AudioClip ImpactSound;
    public AudioClip InvisibilityOnSound;
    public AudioClip InvisibilityOffSound;
    public bool DisablesTrap = false;
    public float TreasureDetectionRadius = 40.0f;
    public GameObject BloodSpill;

    protected AudioSource Audio;
    protected Vector3 StartPoint;
    protected float SigilCooldown = 0.0f;
    protected float InvisibilityTimer = 0.0f;
    protected float InvisibilityCooldown = 0.0f;
    protected float AttackTimer = 0.0f;
    protected float AttackCooldown = 0.0f;
    protected float ActualHealth = 100.0f;
    protected bool IsAlive = true;
    protected bool IsVisible = true;
    protected bool Chanting = false;
    protected bool Moving = false;
    protected bool Running = false;
    protected GameObject Map;
    protected Vector3 LookDirection;


    protected void PlayRandomAttack()
    {
        if (Audio != null && AttackSounds.Length > 0)
        {
            Audio.clip = AttackSounds[Random.Range(0, AttackSounds.Length - 1)];
            Audio.Play();
        }
    }

    protected void PlayRandomHurt()
    {
        if (Audio != null && !Audio.isPlaying && HurtSounds.Length > 0)
        {
            Audio.clip = HurtSounds[Random.Range(0, HurtSounds.Length - 1)];
            Audio.Play();
        }
    }

    protected void PlayDeath()
    {
        if (Audio != null && DeathSound != null)
        {
            Audio.clip = DeathSound;
            Audio.Play();
        }
    }

    virtual protected void FixedStep()
    {

    }

    virtual protected void LastUpdate()
    {

    }

    protected void Init()
    {
        StartPoint = transform.position;
        Audio = GetComponent<AudioSource>();
        ActualHealth = MaxHealth;
        Map = GameObject.FindGameObjectWithTag("Map");
        LookDirection = transform.forward;
    }

    void FixedUpdate()
    {
        FixedStep();

        Quaternion current = transform.rotation;
        Quaternion toward = Quaternion.LookRotation(LookDirection, Vector3.up);

        transform.rotation = Quaternion.RotateTowards(current, toward, 360.0f * Time.fixedDeltaTime);

        if (!IsAlive)
        {
            return;
        }

        AttackTimer -= Time.fixedDeltaTime;
        AttackCooldown -= Time.fixedDeltaTime;
        InvisibilityTimer -= Time.fixedDeltaTime;
        InvisibilityCooldown -= Time.fixedDeltaTime;
        SigilCooldown -= Time.fixedDeltaTime;

        if (Chanting && AttackTimer <= 0.0f)
        {
            Animator anim = GetComponentInChildren<Animator>();
            anim.SetTrigger("Stop");
            Chanting = false;
        }

        if (!IsVisible && InvisibilityTimer <= 0.0f)
        {
            SetVisible(true);
        }
    }

    public void Heal(float heal)
    {
        if (IsAlive)
        {
            ActualHealth = Mathf.Min(ActualHealth + heal, MaxHealth);
        }
    }


    public void Damage(float damage)
    {
        if (!IsAlive)
        {
            return;
        }

        ActualHealth = Mathf.Max(ActualHealth - damage, 0.0f);

        if (BloodSpill != null)
        {
            Instantiate(BloodSpill, transform.position + new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f)), Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up));
        }

        if (ActualHealth == 0.0f)
        {
            IsAlive = false;
            SetVisible(true);
            GetComponent<Collider>().enabled = false;
            GetComponent<CharacterController>().enabled = false;
            GetComponentInChildren<Light>().enabled = false;
            Moving = false;
            Animator anim = GetComponentInChildren<Animator>();
            anim.SetTrigger("Die");
            LastUpdate();
            PlayDeath();
        }
        else
        {
            if (IsAlive)
            {
                PlayRandomHurt();
            }
        }
    }

    public void Respawn()
    {
        ActualHealth = MaxHealth;

        IsAlive = true;
        SetVisible(true);
        GetComponent<Collider>().enabled = true;
        GetComponent<CharacterController>().enabled = true;
        GetComponentInChildren<Light>().enabled = true;
        Moving = false;
        Animator anim = GetComponentInChildren<Animator>();
        anim.SetTrigger("Respawn");
        transform.position = StartPoint;
    }

    public bool GetVisible()
    {
        return IsVisible;
    }

    public void SetVisible(bool visible)
    {
        if (IsVisible != visible)
        {
            IsVisible = visible;
            SkinnedMeshRenderer[] mesh = GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (SkinnedMeshRenderer m in mesh)
            {
                if (IsVisible)
                {
                    m.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                }
                else
                {
                    m.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                }
            }

            if (Audio != null && InvisibilityOnSound != null && InvisibilityOffSound != null)
            {
                if (IsVisible)
                {
                    Audio.clip = InvisibilityOffSound;
                }
                else
                {
                    Audio.clip = InvisibilityOnSound;
                }
                Audio.Play();
            }
        }
    }

    protected bool TurnInvisible()
    {
        if (InvisibilityCooldown <= 0.0f && InvisibilityTime > 0.0f)
        {
            InvisibilityCooldown = InvisibilityTimeout;
            InvisibilityTimer = InvisibilityTime;

            SetVisible(false);

            return true;
        }

        return false;
    }

    public bool IsDead()
    {
        return !IsAlive;
    }

    protected bool CanMove()
    {
        return IsAlive && AttackTimer <= 0.0f;
    }

    protected bool CanAttack()
    {
        return IsAlive && AttackCooldown <= 0.0f;
    }

    protected void Move(bool run)
    {
        if (!Moving || run != Running)
        {
            Running = run;
            Moving = true;
            Animator anim = GetComponentInChildren<Animator>();
            anim.SetTrigger(Running ? "Run" : "Walk");
        }
    }

    protected void Stop()
    {
        if (Moving)
        {
            Animator anim = GetComponentInChildren<Animator>();
            anim.SetTrigger("Stop");
            Moving = false;
        }
    }

    protected bool CanSee(Creature other, out Vector3 dir, float distance = 10000.0f)
    {
        if (!other.GetVisible() && !CanSeeInvisible)
        {
            dir = Vector3.zero;
            return false;
        }

        return InLineOfSight(other.gameObject, out dir, distance);
    }

    protected bool InDirectLineOfSight(GameObject other, out Vector3 dir, float distance)
    {
        dir = other.transform.position - (transform.position + Vector3.up);
        float dist = Mathf.Min(new float[] { dir.magnitude, distance, SightDistance });
        dir.Normalize();
        Ray ray = new Ray(transform.position + Vector3.up, dir);
        RaycastHit hitInfo;

        return (Physics.Raycast(ray, out hitInfo, dist, 1 + (1 << other.layer)) && hitInfo.collider.gameObject == other);
    }

    protected bool InLineOfSight(GameObject other, out Vector3 dir, float distance)
    {
        dir = other.transform.position - transform.position;
        dir.y = 0.0f;
        float dist = Mathf.Min(new float[] { dir.magnitude, distance, SightDistance });
        dir.Normalize();
        Ray ray = new Ray(transform.position + Vector3.up, dir);
        RaycastHit hitInfo;

        return (Physics.Raycast(ray, out hitInfo, dist, 1 + (1 << other.layer)) && hitInfo.collider.gameObject == other);
    }

    void StartProjectile(float damage, Vector3 dir)
    {
        StartCoroutine(ShootProjectile(damage, dir));
    }

    private IEnumerator ShootProjectile(float damage, Vector3 dir)
    {
        yield return new WaitForSeconds(0.25f);
        GameObject obj = (GameObject)Instantiate(Projectile, transform.position + Vector3.up * 1.5f, Quaternion.LookRotation(dir, Vector3.up));
        obj.GetComponent<Rigidbody>().AddForce(dir * ProjectileSpeed, ForceMode.Impulse);
        obj.GetComponent<Arrow>().SetDamage(damage);
    }

    protected bool Attack(Creature other)
    {
        if (other != null && CanMove() && CanAttack())
        {
            if (SigilRune != null && SigilCooldown < 0.0f)
            {
                Sigil sigil = SigilRune.GetComponent<Sigil>();

                Instantiate(SigilRune, other.transform.position, Quaternion.identity);
                Animator anim = GetComponentInChildren<Animator>();
                anim.SetTrigger("Chant");
                Chanting = true;

                AttackDone();
                SigilCooldown = SigilTimeout + sigil.LifeTime;
                AttackTimer += sigil.LifeTime;

                return true;
            }

            float damage = Random.Range(DamageMin, DamageMax);
            Vector3 dir;

            if (CanSee(other, out dir, AttackDistance))
            {
                Animator anim = GetComponentInChildren<Animator>();
                anim.SetTrigger("Attack");
                PlayRandomAttack();
                Moving = false;
                AttackDone();

                if (Projectile != null)
                {
                    StartProjectile(damage, dir);
                }
                else
                {
                    other.Damage(damage);
                }

                return true;
            }
        }
        return false;
    }

    protected void AttackDone()
    {
        AttackTimer = AttackTime;
        AttackCooldown = AttackTimeout;
    }

    void StartMeleeAttack()
    {
        StartCoroutine(MeleeAttack());
    }

    private IEnumerator MeleeAttack()
    {
        yield return new WaitForSeconds(0.2f);
        List<Creature> enemies = new List<Creature>();

        foreach (GameObject opponent in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if ((opponent.transform.position - transform.position).sqrMagnitude < AttackDistance * AttackDistance)
            {
                Creature other = opponent.GetComponent<Creature>();

                if (other != null && !other.IsDead())
                {
                    enemies.Add(other);
                }
            }
        }

        if (enemies.Count > 0)
        {
            float factor = Mathf.Max(enemies.Count - 1, 1);
            foreach(Creature enemy in enemies)
            {
                float damage = Random.Range(DamageMin, DamageMax) / factor;
                enemy.Damage(damage);
            }
            float pseudoDamage = Random.Range(DamageMin, DamageMax);
            Heal(pseudoDamage * VampireFactor);

            Audio.clip = ImpactSound;
            Audio.Play();
        }
    }

    protected void AttackInPlace(Vector3 direction, bool shoot)
    {
        if (CanMove() && CanAttack())
        {
            float damage = Random.Range(DamageMin, DamageMax);
            AttackDone();

            if (Projectile != null && shoot)
            {
                Animator anim = GetComponentInChildren<Animator>();
                anim.SetTrigger("Shoot");

                StartProjectile(damage, direction);
            }
            else
            {
                Animator anim = GetComponentInChildren<Animator>();
                anim.SetTrigger("Attack");
                PlayRandomAttack();

                StartMeleeAttack();
            }
        }
    }

}
