﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : Creature
{
    public GameObject Trap;
    public GameObject Treasure;
    public int TrapCount = 3;
    public int NumberOfLives = 3;
    public float RespawnTime = 10.0f;

    protected int Lives;
    protected float RespawnTimer;
    protected bool Slowed = false;
    protected int Traps;
    protected RawImage Healthbar;
    protected Text Counter;
    protected float CameraRotation;

    // Use this for initialization
    void Start()
    {
        Traps = TrapCount;
        Healthbar = GameObject.FindGameObjectWithTag("Healthbar").GetComponentInChildren<RawImage>();
        Counter = GameObject.Find("Counter").GetComponentInChildren<Text>();
        Lives = NumberOfLives;

        Init();
    }

    public void Slow()
    {
        Slowed = true;
    }

    public void SetCameraRotation(float rotation)
    {
        CameraRotation = rotation;
    }

    protected override void LastUpdate()
    {
        Healthbar.rectTransform.localScale = new Vector3(ActualHealth / MaxHealth, 1.0f, 1.0f);
        if (!IsAlive)
        {
            RespawnTimer = RespawnTime;
        }
    }

    protected override void FixedStep()
    {
        Healthbar.rectTransform.localScale = new Vector3(ActualHealth / MaxHealth, 1.0f, 1.0f);

        if (!IsAlive)
        {
            RespawnTimer -= Time.fixedDeltaTime;
            if (RespawnTimer < 0.0f)
            {
                Map map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
                if (Lives > 0 && map)
                {
                    Lives--;
                    Respawn();
                    Counter.text = "Traps: " + Traps + " / " + TrapCount + "\nLives: " + Lives;
                }
                else
                {
                    if (map)
                    {
                        map.Finish(false);
                        RespawnTimer = 10000000.0f; // Ehh...
                    }
                }
            }

            return;
        }

        if (transform.position.y < 0.0f)
        {
            Vector3 pos = transform.position;
            pos.y = 0.1f;
            transform.position = pos;
        }

        float horizontal = Mathf.Clamp(Input.GetAxis("Horizontal"), -1, 1);
        float vertical = Mathf.Clamp(Input.GetAxis("Vertical"), -1, 1);

        if (Input.GetAxis("Fire1") > 0.0f && CanAttack())
        {
            TurnTowardsCursor();
            AttackInPlace(LookDirection, false);
        }

        if (Input.GetAxis("Fire2") > 0.0f && CanAttack())
        {
            TurnTowardsCursor();
            AttackInPlace(LookDirection, true);
        }

        if (Input.GetAxis("Trap") > 0.0f && CanAttack() && Traps > 0)
        {
            UseTrap();
        }

        if (Input.GetAxis("Scare") > 0.0f && CanAttack())
        {
            UseScare();
        }

        if (Input.GetAxis("Treasure") > 0.0f && CanAttack() && Traps > 0)
        {
            UseTreasure();
        }

        if (Input.GetAxis("Invisibility") > 0.0f)
        {
            TurnInvisible();
        }

        if (CanMove() && (Mathf.Abs(horizontal) > 0.0f || Mathf.Abs(vertical) > 0.0f))
        {
            Move(true);
            Vector3 targetDirection = Quaternion.AngleAxis(CameraRotation, Vector3.up) * new Vector3(horizontal, 0f, vertical);
            targetDirection.Normalize();
            GetComponent<CharacterController>().Move(targetDirection * Speed * Time.fixedDeltaTime * (Slowed ? 0.66f : 1.0f) - Vector3.up);
            LookDirection = targetDirection;
        }
        else
        {
            TurnTowardsCursor();
            Stop();
        }

        Slowed = false;
    }

    private void TurnTowardsCursor()
    {
        Vector3 near = Input.mousePosition;
        near.z = 10.0f;
        near = Camera.main.ScreenToWorldPoint(near);
        Vector3 far = Input.mousePosition;
        far.z = 20.0f;
        far = Camera.main.ScreenToWorldPoint(far);
        Vector3 dir = far - near;
        dir.Normalize();

        Ray ray = new Ray(near, dir);
        RaycastHit info;

        if (Physics.Raycast(ray, out info, 100000.0f, 1))
        {
            dir = near + dir * info.distance - transform.position;
            dir.y = 0.0f;
            dir.Normalize();
            LookDirection = dir;
        }
    }

    private void UseTreasure()
    {
        Instantiate(Treasure, transform.position, transform.rotation);
        AttackDone();
        AttackTimer *= 0.33f;
    }

    private void UseTrap()
    {
        Instantiate(Trap, transform.position, transform.rotation);
        AttackDone();
        AttackTimer *= 0.33f;
    }

    private void UseScare()
    {
        // Unimplemented
        //AttackDone();
    }

    public void PlaceTrap()
    {
        Traps--;

        Counter.text = "Traps: " + Traps + " / " + TrapCount + "\nLives: " + Lives;
    }

    public void ReleaseTrap()
    {
        Traps++;

        Counter.text = "Traps: " + Traps + " / " + TrapCount + "\nLives: " + Lives;
    }
}
