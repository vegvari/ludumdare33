﻿using UnityEngine;
using System.Collections;

public class BirdviewCamera : MonoBehaviour
{
    public float MinZoom = 10.0f;
    public float MaxZoom = 60.0f;

    private float Zoom = 20.0f;
    private float Angle = 60.0f;
    private float Rotation = 0.0f;

    protected GameObject FollowedObject;

    // Use this for initialization
    void Start()
    {
        FollowedObject = null;
        Rotation = Random.Range(0, 4) * 90.0f;
    }

    // Update is called once per frame
    void Update()
    {
        //if (FollowedObject == null)
        {
            FollowedObject = GameObject.FindGameObjectWithTag("Player");
        }


        if (FollowedObject)
        {
            Zoom -= Input.mouseScrollDelta.y;
            Zoom = Mathf.Clamp(Zoom, MinZoom, MaxZoom);

            Quaternion rotation = Quaternion.AngleAxis(Rotation, Vector3.up) * Quaternion.AngleAxis(Angle, Vector3.right);
            Vector3 vec = (rotation * Vector3.forward) * Zoom;
            FollowedObject.GetComponent<Player>().SetCameraRotation(Rotation);

            transform.position = FollowedObject.transform.position - vec;
            transform.localRotation = rotation;
        }
    }
}
